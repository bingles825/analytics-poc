Desired Features

- Granular customization (test with predictive analytics showing ligher bars for future)
- Zoom features
- Support native d3 scales

1. Types of charts
   a. Pie chart
   b. Facet Chart
   c. Time chart
2. Feature set:
   a. Drill downs
   b. Drill down to timespan
   c. Drill down to a transactional grid level
   d. Zoom in and Zoom Out
   e. Select a specific time period
   f. Export
   i. Export summary data to pdf, png, excel
   ii. Export the transactional dataset behind the chart at the lowest level to excel
   g. Ability to view the data by specific time span (by day, by week, by months etc.,)
   h. Ability to view full screen
3. Speed and performance w.r.t to large data sets (1 million rows)
4. Multi-touch gestures for Mobile devices
