import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/system';
import { App } from './components';
import {
  API,
  composeProviders,
  SessionService,
  SessionServiceProvider,
  ValuesType,
  WidgetDataStore,
  WidgetDataServiceProvider,
  WidgetStore,
  WidgetStoreProvider,
} from './services';
import reportWebVitals from './reportWebVitals';
import { theme } from './theme';

window.ZoomChartsLicense = 'ZCP-9o6jbxq7v: WhiteSpaceHealth.com';
window.ZoomChartsLicenseKey =
  '75956070945cdb7240026197603ab645181bae713f4d93e6f2' +
  '3f792d3615912eab874c458879573036836880298ae87d062abf187ef6aabfeeff27c87f23faa' +
  '27136616e80fd9440b02886ad4456ad73c32a16bc16c8a68515b6a94eee62abdbe01d5a224b38' +
  'd02cf344177dae930a4b5c05a25438b940fa472bd7743dfc205c39c98b4dbc26ee71126a4f8c6' +
  '70a708aa8f13637fa05a0c92d873a5e9e66437fa8ac879a87a6b7d144f335a73d22ff30b4d2ae' +
  'a9cf111a897dfaa251570c83c31c3c50b312b045f916184b76d92c7144247a8bdf48f39d300d6' +
  '798e9d2bebdff4522f0fa33d55c89ed84e0340b058cdd03dd9a12ae22914d57767f9eb514f6a9';

const apiUrlRoot = '/api/WSC/V1';
const authUrlRoot = '/api/Login/V1';

if (
  process.env.NODE_ENV === 'development' &&
  window.location.hostname === 'http://192.168.1.201'
) {
  const { createWorker } = require('./mocks/browser');
  const worker = createWorker(`http://192.168.1.201:3000${apiUrlRoot}`);
  worker.start({
    onUnhandledRequest: 'bypass',
  });
}

// Create our history object explicitly so we can share it with our API + Router
const history = createBrowserHistory();

// Compose a context provider for our explicit contexts
const ContextProvider = composeProviders(
  SessionServiceProvider,
  WidgetDataServiceProvider,
  WidgetStoreProvider,
);

let widgetStore: WidgetStore;
let widgetDataStore: WidgetDataStore;

const api = new API(apiUrlRoot, authUrlRoot, (response) => {
  if (response.status === 401 || response.status === 403) {
    history.push('/login');

    // reset any state that was previously loaded. This is especially helpful
    // for clearing any errors that were cached.
    widgetStore.init();
    widgetDataStore.init();
  }
});

widgetStore = new WidgetStore(api);
widgetDataStore = new WidgetDataStore(api);

const contextValues = [
  new SessionService(api),
  (widgetDataStore = new WidgetDataStore(api)),
  (widgetStore = new WidgetStore(api)),
] as ValuesType<typeof ContextProvider>;

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router history={history}>
        <ContextProvider values={contextValues}>
          <App />
        </ContextProvider>
      </Router>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
