import { setupWorker } from 'msw';
import { createHandlers } from './handlers';

export function createWorker(apiRootUrl: string) {
  return setupWorker(...createHandlers(apiRootUrl));
}
