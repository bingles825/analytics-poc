import { rest } from 'msw';
import { getData, getObject } from './data/daily-visits-and-total-bills';

export function createHandlers(apiRootUrl: string) {
  return [
    // rest.options('https://test.wshcloud.com/api/WSC/V1/*', (req, res, ctx) => {
    //   console.log('OPTIONS');
    //   // const response = await ctx.fetch(req);

    //   return res(
    //     ctx.status(200),
    //     ctx.set('Allow', 'OPTIONS, GET, HEAD, POST'),
    //     ctx.set('Access-Control-Allow-Origin', '*'),
    //     // ctx.json({ ...response }),
    //   );
    // }),

    rest.get(`${apiRootUrl}/*`, (req, res, ctx) => {
      const [, action] = req.url.pathname.match(/([^/]+)$/) ?? [];

      for (const prefix of [
        'DailyVisitsAndTotalBills',
        'DailyBilllVolumevsCompletedvsPending',
      ] as const) {
        if (action === 'GetObject') {
          const namePrefix = `?name=${prefix}`;

          if (req.url.search.startsWith(namePrefix)) {
            const suffix = req.url.search.replace(
              namePrefix,
              '',
            ) as keyof typeof getObject[typeof prefix];

            const data = getObject[prefix][suffix];

            if (data) {
              return res(ctx.json(data));
            }
          }
        } else if (action === 'GetData') {
          const queryPrefix = `?Query=${prefix}`;

          if (req.url.search.startsWith(queryPrefix)) {
            const suffix = decodeURIComponent(
              req.url.search.replace(queryPrefix, ''),
            ) as keyof typeof getData[typeof prefix];

            const data = getData[prefix][suffix];

            if (data) {
              return res(ctx.json(data));
            }
          }
        }
      }

      console.log(`${req.url.href} not found.`);

      return res(ctx.status(404));
    }),
  ];
}

// function parseParams<TParams extends Record<string, unknown>>(
//   search: string,
// ): TParams {
//   return search
//     .substr(1)
//     .split('&')
//     .reduce((result, pair) => {
//       const [key, value] = pair.split('=');
//       result[key as keyof TParams] = value as TParams[typeof key];
//       return result;
//     }, {} as TParams);
// }
