# Example data URLs

/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsWidget&type=Widget
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyTransactionsDef&type=Definition x 5
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbySpecialityDef&type=Definition
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyProviderDef&type=Definition x 3
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyPracticeDef&type=Definition
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyPayerNameDef&type=Definition
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyPayerCategoryDef&type=Definition
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDrillDownbyFacilityDef&type=Definition
/api/WSC/V1/GetObject?name=DailyVisitsAndTotalBillsDef&type=Definition x 2
/api/WSC/V1/GetData?Query=DailyVisitsAndTotalBillsQuery&Parameters=%5B%7B%22name%22%3A%22SourceSystem%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Enterprise%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Practice%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Facility%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Speciality%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Provider%22%2C%22type%22%3A%22multi%22%2C%22value%22%3A%5B%5D%7D%2C%7B%22name%22%3A%22Time%22%2C%22type%22%3A%22dateRange%22%2C%22value%22%3A%5B%222019-09-03%22%2C%222020-09-03%22%5D%7D%5D&DBCacheID=4ba2a302-f856-4921-b06b-ede34ec03185&FilterName=&CacheID=0910b779-ff05-497e-85c4-37013587a2a2
