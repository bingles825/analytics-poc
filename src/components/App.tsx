import React from 'react';
import { styled } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import { Route } from 'react-router-dom';
import { Login, Widget } from '.';
import { WidgetName } from '../data';
import '@fontsource/nunito-sans';
import { useSessionService } from '../services';

const StyledRoot = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  '& nav': {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

const StyledWidgetLayout = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  '& > *': {
    flexBasis: '50%',
  },
  '& > :nth-of-type(3)': {
    flexBasis: '100%',
  },
});

const widgetNames: WidgetName[] = [
  'DailyVisitsAndTotalBillsWidget',
  'DailyBilllVolumevsCompletedvsPendingWidget',
  // 'BillLagandSubmissionlagWidget',
  // 'DailyVisitsAndTotalBillsWidget',
  // 'DailyVisitsAndTotalBillsWidget',
  // 'UnbilledChargesWidget',
];
export interface AppProps {
  className?: string;
}

const App: React.FC<AppProps> = ({ className }) => {
  const { logout } = useSessionService();

  const onLogoutClick = React.useCallback(() => {
    void logout();
  }, [logout]);

  return (
    <StyledRoot className={className}>
      <Route exact path="/">
        <nav>
          <button onClick={onLogoutClick}>Logout</button>
        </nav>
        <StyledWidgetLayout>
          {widgetNames.map((name, i) => (
            <React.Fragment key={`${name}-${i}`}>
              <Widget
                widgetName={name}
                variation="zoom"
                chartAspectRatio={{
                  width: 800 * (i === 2 ? 2 : 1),
                  height: 400,
                }}
              />
              {/* <Widget widgetName={name} variation="recharts" />
              <Widget widgetName={name} variation="nivo" /> */}
            </React.Fragment>
          ))}
        </StyledWidgetLayout>
      </Route>
      <Route path="/login" component={Login} />
    </StyledRoot>
  );
};

export default observer(App);
