import React from 'react';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import HighlightOff from '@mui/icons-material/HighlightOff';
import FilterAlt from '@mui/icons-material/FilterAlt';
import { observer } from 'mobx-react-lite';
import {
  BarSeriesClickData,
  FacetChartData,
  FacetChartDefinition,
  FilterParam,
  TimeChartData,
  TimeChartDefinition,
  WidgetName,
  WidgetStackItem,
} from '../data';
import { useWidgetDataService, useWidgetStore } from '../services';
import { ObservableStack } from '../services';

import { Nivo, Recharts, Victory, Visx, Zoom } from '../chartLib';

const StyledRoot = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  padding: 20,
});

const StyledToolbar = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-end',
});

const mockFilters: FilterParam[][] = [
  [
    { name: 'SourceSystem', type: 'multi', value: [] },
    { name: 'Enterprise', type: 'multi', value: [] },
    { name: 'Practice', type: 'multi', value: [] },
    { name: 'Facility', type: 'multi', value: [] },
    { name: 'Speciality', type: 'multi', value: [] },
    { name: 'Provider', type: 'multi', value: [] },
    { name: 'Time', type: 'dateRange', value: ['2019-09-03', '2020-09-03'] },
  ],
  [
    { name: 'SourceSystem', type: 'multi', value: [] },
    { name: 'Enterprise', type: 'multi', value: [] },
    { name: 'Practice', type: 'multi', value: [] },
    { name: 'Facility', type: 'multi', value: [] },
    { name: 'Speciality', type: 'multi', value: [] },
    { name: 'Provider', type: 'multi', value: [] },
    { name: 'Time', type: 'dateRange', value: ['2020-08-21', '2020-08-21'] },
  ],
  [
    { name: 'SourceSystem', type: 'multi', value: [] },
    { name: 'Enterprise', type: 'multi', value: [] },
    { name: 'Practice', type: 'multi', value: ['PracticeCategory112'] },
    { name: 'Facility', type: 'multi', value: [] },
    { name: 'Speciality', type: 'multi', value: [] },
    { name: 'Provider', type: 'multi', value: [] },
    { name: 'Time', type: 'dateRange', value: ['2020-08-21', '2020-08-21'] },
  ],
];

export interface WidgetProps {
  className?: string;
  widgetName: WidgetName;
  variation: 'recharts' | 'nivo' | 'victory' | 'visx' | 'zoom';
  chartAspectRatio: { width: number; height: number };
}

const variationMap = {
  nivo: Nivo.NivoTimeSeriesChart,
  recharts: Recharts.RechartsTimeSeriesChart,
  victory: Victory.TimeSeriesChart,
  visx: Visx.TimeSeriesChart,
  zoom: Zoom.TimeSeriesChart,
} as const;

const Widget: React.FC<WidgetProps> = ({
  className,
  widgetName,
  variation,
  chartAspectRatio,
}) => {
  const TimeSeriesChart = variationMap[variation];

  const { getWidgetObject, getWidgetDefinition } = useWidgetStore();
  const { getData } = useWidgetDataService();
  const [drilldownStack] = React.useState(
    () => new ObservableStack<WidgetStackItem>(),
  );

  const [barClicked, setBarclicked] =
    React.useState<BarSeriesClickData<unknown> | null>(null);

  const widgetObject = getWidgetObject(widgetName);

  React.useEffect(() => {
    if (widgetObject && drilldownStack.isEmpty()) {
      drilldownStack.push({
        definitionName: widgetObject.definition,
        filters: mockFilters[0],
      });
    }
  }, [widgetObject, drilldownStack]);

  const stackItem = drilldownStack.getTop();

  const widgetDefinition = stackItem
    ? getWidgetDefinition(stackItem.definitionName)
    : null;

  const data = widgetDefinition
    ? getData(widgetDefinition.def.data.query, stackItem!.filters)
    : null;

  const onDrillup = React.useCallback(() => {
    if (drilldownStack.getSize() > 1) {
      drilldownStack.pop();
    }
  }, [drilldownStack]);

  return (
    <StyledRoot className={className}>
      <StyledToolbar>
        <IconButton>
          <FilterAlt />
        </IconButton>
        {drilldownStack.getSize() > 1 ? (
          <IconButton onClick={onDrillup}>
            <HighlightOff />
          </IconButton>
        ) : null}
      </StyledToolbar>
      {widgetObject &&
        widgetDefinition &&
        (widgetDefinition.defSubType === 'TimeChart' ? (
          <TimeSeriesChart
            title={`${widgetObject.title} (${variation})`}
            definition={widgetDefinition.def as TimeChartDefinition}
            data={data as TimeChartData}
            aspectRatio={chartAspectRatio}
            onBarClick={setBarclicked}
          />
        ) : (
          <Recharts.RechartsFacetChart
            title={widgetObject.title}
            definition={widgetDefinition.def as FacetChartDefinition}
            data={data as FacetChartData}
            onBarClick={setBarclicked}
          />
        ))}
      {widgetDefinition && barClicked && (
        <Menu
          onContextMenu={(event) => {
            event.preventDefault(); // This stops the default context menu from triggering from the overlay which covers the full screen
            setBarclicked(null);
          }}
          anchorReference="anchorPosition"
          anchorPosition={
            barClicked
              ? { top: barClicked.clientY, left: barClicked.clientX }
              : undefined
          }
          open={barClicked != null}
          onClose={() => setBarclicked(null)}>
          {widgetDefinition.drilldown.map(({ prompt, def }) => (
            <MenuItem
              key={prompt}
              onClick={() => {
                setBarclicked(null);
                drilldownStack.push({
                  definitionName: def,
                  filters: mockFilters[drilldownStack.getSize()],
                });
              }}>
              {prompt}
            </MenuItem>
          ))}
        </Menu>
      )}
    </StyledRoot>
  );
};

export default observer(Widget);
