import React from 'react';
import { useHistory } from 'react-router';
import { styled } from '@mui/material/styles';
import { useSessionService } from '../services';

const StyledRoot = styled('form')({});

export interface LoginProps {
  className?: string;
}

const Login: React.FC<LoginProps> = ({ className }) => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const history = useHistory();
  const sessionService = useSessionService();

  const onUsernameChange = React.useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLInputElement>) => {
      setUsername(currentTarget.value);
    },
    [],
  );

  const onPasswordChange = React.useCallback(
    ({ currentTarget }: React.ChangeEvent<HTMLInputElement>) => {
      setPassword(currentTarget.value);
    },
    [],
  );

  const onSubmit = React.useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();

      const ok = await sessionService.login({ username, password });
      if (ok) {
        history.push('/');
      }
    },
    [history, password, sessionService, username],
  );

  return (
    <StyledRoot className={className} onSubmit={onSubmit}>
      <label>
        Username
        <input type="text" value={username} onChange={onUsernameChange} />
      </label>

      <label>
        Password
        <input type="password" value={password} onChange={onPasswordChange} />
      </label>

      <button type="submit">Login</button>
    </StyledRoot>
  );
};

export default Login;
