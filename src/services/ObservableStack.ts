import { makeAutoObservable } from 'mobx';

/**
 * An observable stack data structure
 */
export class ObservableStack<T> {
  constructor() {
    makeAutoObservable(this);
  }

  private stack: T[] = [];

  isEmpty = (): boolean => {
    return this.stack.length === 0;
  };

  push = (stackItem: T): T => {
    this.stack.push(stackItem);
    return stackItem;
  };

  pop = (): T | null => {
    return this.stack.pop() ?? null;
  };

  getSize = (): number => {
    return this.stack.length;
  };

  getTop = (): T | null => {
    return this.stack.length === 0 ? null : this.stack[this.stack.length - 1];
  };
}
