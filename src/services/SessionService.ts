import { API, createExplicitContext } from '.';

export class SessionService {
  constructor(private api: API) {}
  // https://test.wshcloud.com/api/Login/V1/Login

  login = async (credentials: {
    username: string;
    password: string;
  }): Promise<boolean> => {
    return this.api.login(credentials);
  };

  logout = async (): Promise<void> => {
    return this.api.logout();
  };
}

export const {
  Provider: SessionServiceProvider,
  useContext: useSessionService,
} = createExplicitContext<SessionService>('SessionService');
