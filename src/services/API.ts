import { FilterParam, ObjectName, ObjectType, QueryName } from '../data';
import { CSRFStore } from './CSRFStore';

export class API {
  constructor(
    private apiRootUrl: string,
    private authRootUrl: string,
    private handleError: (response: Response) => void,
  ) {}

  csrf = new CSRFStore();

  login = async (credentials: {
    username: string;
    password: string;
  }): Promise<boolean> => {
    await this.logout();

    this.csrf.token = await this.getCSRFToken();

    const response = await fetch(`${this.authRootUrl}/Login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': this.csrf.token,
      },
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify(credentials),
    });

    if (response.status === 401) {
      return false;
    }

    // refresh our CSRF token so that it matches our session cookie
    this.csrf.token = await this.getCSRFToken();

    return true;
  };

  logout = async (): Promise<void> => {
    sessionStorage.clear();
    document.cookie = '.WSC-APP= ; expires = Thu, 01 Jan 1970 00:00:00 GMT';

    if (window.location.pathname !== '/login') {
      window.location.pathname = '/login';
    }
  };

  /** Get a CSRF token from the backend API */
  getCSRFToken = async (): Promise<string> => {
    const csrfResponse = await fetch(`${this.authRootUrl}/Init`);
    const { CSRF } = await csrfResponse.json();
    return CSRF;
  };

  /** Retrieve an Object by name and type from the backend API */
  getObject = async <TResult>(
    name: ObjectName,
    type: ObjectType,
  ): Promise<TResult | null> => {
    const response = await fetch(
      `${this.apiRootUrl}/GetObject?name=${name}&type=${type}`,
      {
        headers: {
          'X-CSRF-TOKEN': this.csrf.token,
        },
      },
    );

    if (response.status >= 400) {
      this.handleError(response);
      return null;
    }

    return response.json();
  };

  /** Get data for a given query + filters. */
  getData = async <TResult>(
    queryName: QueryName,
    filterParams: FilterParam[],
  ): Promise<TResult | null> => {
    const filterParamsStr = encodeURIComponent(JSON.stringify(filterParams));
    const response = await fetch(
      `${this.apiRootUrl}/GetData?Query=${queryName}&Parameters=${filterParamsStr}`,
      {
        headers: {
          'X-CSRF-TOKEN': this.csrf.token,
        },
        mode: 'cors',
        credentials: 'include',
      },
    );

    if (response.status >= 400) {
      this.handleError(response);
      return null;
    }

    return response.json();
  };
}
