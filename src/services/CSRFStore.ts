/**
 * Session storage for the CSRF token.
 */
export class CSRFStore {
  private static readonly key = 'CSRF_TOKEN';

  get token() {
    return sessionStorage.getItem(CSRFStore.key) ?? '';
  }

  set token(token: string) {
    sessionStorage.setItem(CSRFStore.key, token);
  }

  clear = () => {
    sessionStorage.removeItem(CSRFStore.key);
  };
}
