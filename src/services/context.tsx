import React from 'react';

export interface CreateExplicitContextResult<TValue> {
  Provider: React.Context<TValue>['Provider'];
  useContext: () => TValue;
}

/**
 * Create a React Context that requires a value be provided explicitly (aka. no
 * default value). Returns the Context Provider + a useContext hook that
 * encapsulates access to the Context.
 */
export function createExplicitContext<TValue>(
  name: string,
): CreateExplicitContextResult<TValue> {
  const Context = React.createContext<TValue | null>(null);

  function useContext(): TValue {
    const value = React.useContext(Context);

    if (value === null) {
      throw new Error(`Context value is null for '${name}'.`);
    }

    return value;
  }

  return {
    Provider: Context.Provider as React.Context<TValue>['Provider'],
    useContext,
  };
}

type ComposedContextValues<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  TProviders extends React.Context<any>['Provider'][],
> = {
  [P in keyof TProviders]: TProviders[P] extends React.Context<
    infer V
  >['Provider']
    ? V
    : never;
};

export interface ComposeProvidersProps<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  TProviders extends React.Context<any>['Provider'][],
> {
  values: ComposedContextValues<TProviders>;
}

/**
 * Compose multiple Context Providers into one.
 */
export function composeProviders<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  TProviders extends React.Context<any>['Provider'][],
>(...providers: TProviders) {
  return ({
    values,
    children,
  }: React.PropsWithChildren<
    ComposeProvidersProps<TProviders>
  >): JSX.Element => {
    function child(i: number) {
      if (i < providers.length) {
        const Provider = providers[i];
        return <Provider value={values[i]}>{child(i + 1)}</Provider>;
      }
      return children;
    }

    return <>{child(0)}</>;
  };
}

/**
 * Helper to extract values type from props of React.FC
 */
export type ValuesType<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  TProvider extends (props: { values: any }) => JSX.Element,
> = TProvider extends (props: { values: infer TProps }) => JSX.Element
  ? TProps
  : never;
