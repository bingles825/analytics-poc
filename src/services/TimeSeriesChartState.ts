import * as d3 from 'd3';
import { makeAutoObservable } from 'mobx';
import { Milliseconds, TimeChartData, TimeChartDefinition } from '../data';
import {
  calculateSpanInMs,
  mapArrayKey,
  parseUnitSpec,
  ticks,
  times,
} from '../util';

type DomainTuple = [number, number] | [Date, Date];

interface DomainXY {
  x: DomainTuple;
  y: DomainTuple;
}

const yTickCount = 5;

export class TimeSeriesChartState {
  constructor(
    definition: TimeChartDefinition,
    chartData: TimeChartData | null,
  ) {
    makeAutoObservable(this);

    this.definition = definition;
    const unitSpec = parseUnitSpec(definition.navigation.initialDisplayUnit);
    this.xIntervalMs = calculateSpanInMs(unitSpec);
    this.seriesCount = chartData?.querySchema.length ?? 0;

    // adjust our values from seconds to milliseconds
    this.dataAll = chartData?.values.map(mapArrayKey(0, times(1000))) ?? [];

    this.setZoomDomain();
  }

  private definition: TimeChartDefinition;
  // private unitSpec: UnitSpec;
  private xIntervalMs: Milliseconds;
  private containerWidth: number = 0;
  private seriesCount: number;
  private dataAll: TimeChartData['values'];
  private dataZoomed!: TimeChartData['values'];
  private zoomDomain!: DomainXY;

  getContainerWidth = (): number => {
    return this.containerWidth;
  };

  setContainerWidth = (width: number): void => {
    this.containerWidth = width;
  };

  getBarData = () => {
    const zoomDomainSpan =
      Number(this.zoomDomain.x[1]) - Number(this.zoomDomain.x[0]);

    const groupWidth =
      this.containerWidth / (zoomDomainSpan / this.xIntervalMs);

    const barWidth = (groupWidth / this.seriesCount) * 0.6;
    const groupOffset = barWidth;

    return {
      barWidth,
      groupOffset,
    };
  };

  getDataZoomed = () => {
    return this.dataZoomed;
  };

  getXDomain = (): DomainTuple | undefined => {
    if (this.dataAll.length === 0) {
      return undefined;
    }

    const padding = this.xIntervalMs / 2;

    return [
      this.dataAll[0][0],
      this.dataAll[this.dataAll.length - 1][0] + padding,
    ];
  };

  getXAxisTicks = (): number[] => {
    const domain = this.getXDomain();
    return domain ? [...ticks(domain, this.xIntervalMs)] : [];
  };

  getYAxisTicks = (): number[] => {
    const zoomDomain = this.getZoomDomain();

    const ticks = zoomDomain
      ? d3
          .scaleLinear()
          .domain(zoomDomain.y as [number, number])
          .ticks(yTickCount)
      : [];

    return ticks;
  };

  getZoomDomain = (): DomainXY => {
    return this.zoomDomain;
  };

  setZoomDomain = (x?: DomainTuple): void => {
    if (x == null) {
      const padding = this.xIntervalMs / 2;

      const xMax = this.dataAll.length
        ? this.dataAll[this.dataAll.length - 1][0]
        : 1;

      let xMin = this.dataAll.length ? this.dataAll[0][0] : 0;

      if (this.definition.navigation.initialDisplayPeriod) {
        xMin =
          xMax -
          calculateSpanInMs(
            parseUnitSpec(this.definition.navigation.initialDisplayPeriod),
          );
      }

      x = [xMin - padding, xMax + padding];
    }

    this.dataZoomed = this.dataAll.filter(([ms]) => ms >= x![0] && ms <= x![1]);

    const [yMin = 0, yMax = 1] = d3.extent(this.dataZoomed, (datum) =>
      Math.max(datum[1], datum[2]),
    );

    this.zoomDomain = {
      x,
      // using d3.nice will give us a range that is more friendly to our desired tick count
      y: d3.nice(Math.min(yMin, 0), yMax, yTickCount),
    };
  };

  onZoomDomainChange = (domain?: Partial<DomainXY>) => {
    this.setZoomDomain(domain?.x);
  };
}
