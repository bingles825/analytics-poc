import { makeAutoObservable, runInAction } from 'mobx';
import { API } from '.';
import {
  WidgetObject,
  WidgetName,
  WidgetDefinition,
  WidgetDefinitionName,
  WidgetDrilldownDefinitionName,
} from '../data';
import { createExplicitContext } from './context';

export class WidgetStore {
  constructor(private api: API) {
    makeAutoObservable(this);
    this.init();
  }

  private isLoadingWidget!: PartialRecord<WidgetName, boolean>;
  private isLoadWidgetError!: PartialRecord<WidgetName, boolean>;
  private isLoadingWidgetDefinitions!: PartialRecord<
    WidgetDefinitionName,
    boolean
  >;

  private widgets!: PartialRecord<WidgetName, WidgetObject>;
  private widgetDefinitions!: PartialRecord<
    WidgetDefinitionName | WidgetDrilldownDefinitionName,
    WidgetDefinition
  >;

  init = () => {
    this.isLoadingWidget = {};
    this.isLoadWidgetError = {};
    this.isLoadingWidgetDefinitions = {};
    this.widgets = {};
    this.widgetDefinitions = {};
  };

  getWidgetObject = (widgetName: WidgetName): WidgetObject | null => {
    const object = this.widgets[widgetName] ?? null;

    if (!object) {
      // If we don't yet have a widget object, load it
      void this.loadWidget(widgetName);
    }

    return object;
  };

  getWidgetDefinition = (
    definitionName: WidgetDefinitionName | WidgetDrilldownDefinitionName,
  ): WidgetDefinition | null => {
    return (
      this.widgetDefinitions[definitionName as WidgetDefinitionName] ?? null
    );
  };

  loadWidget = async (widgetName: WidgetName): Promise<void> => {
    // If we are already loading then bail
    if (
      this.isLoadingWidget[widgetName] ||
      this.isLoadWidgetError[widgetName]
    ) {
      return;
    }

    runInAction(() => {
      this.isLoadingWidget[widgetName] = true;
    });

    const widgetObject = await this.api.getObject<WidgetObject>(
      widgetName,
      'Widget',
    );

    if (widgetObject) {
      runInAction(() => {
        this.widgets[widgetObject.name as WidgetName] = widgetObject;
      });

      await this.loadWidgetDefinitions(widgetObject.definition);
    } else {
      runInAction(() => {
        this.isLoadWidgetError[widgetName] = true;
      });
    }

    runInAction(() => {
      this.isLoadingWidget[widgetName] = false;
    });
  };

  loadWidgetDefinitions = async (
    definitionName: WidgetDefinitionName,
  ): Promise<void> => {
    // If we are already loading this definition then bail
    if (this.isLoadingWidgetDefinitions[definitionName]) {
      return;
    }

    runInAction(() => {
      this.isLoadingWidgetDefinitions[definitionName] = true;
    });

    const queue: (WidgetDefinitionName | WidgetDrilldownDefinitionName)[] = [
      definitionName,
    ];

    const tally: Record<string, number> = {};

    // recursively load widget definitions
    while (queue.length > 0) {
      const current = queue.shift()!;

      if (this.widgetDefinitions[current]) {
        continue;
      }

      tally[current] = (tally[current] ?? 0) + 1;
      const widgetDef = await this.api.getObject<WidgetDefinition>(
        current,
        'Definition',
      );

      if (!widgetDef) {
        continue;
      }

      widgetDef.drilldown?.forEach(({ def }) => {
        queue.push(def);
      });

      runInAction(() => {
        this.widgetDefinitions[widgetDef.name] = widgetDef;
      });
    }

    runInAction(() => {
      this.isLoadingWidgetDefinitions[definitionName] = false;
    });
  };
}

export const { Provider: WidgetStoreProvider, useContext: useWidgetStore } =
  createExplicitContext<WidgetStore>('WidgetStore');
