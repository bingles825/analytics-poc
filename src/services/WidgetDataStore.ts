import { makeAutoObservable, runInAction } from 'mobx';
import { API, createExplicitContext } from '.';
import {
  QueryName,
  FilterParam,
  ChartData,
  DrilldownQueryName,
  FacetChartData,
  TimeChartData,
} from '../data';

/**
 * State store for widget data.
 */
export class WidgetDataStore {
  constructor(private api: API) {
    makeAutoObservable(this);
    this.init();
  }

  private isLoading!: PartialRecord<DataStoreKey, boolean>;
  private isError!: PartialRecord<DataStoreKey, boolean>;
  private data!: PartialRecord<DataStoreKey, ChartData>;

  init = () => {
    this.isLoading = {};
    this.isError = {};
    this.data = {};
  };

  getData = <
    TQueryName extends QueryName,
    TData extends TQueryName extends DrilldownQueryName
      ? FacetChartData
      : TimeChartData,
  >(
    queryName: TQueryName,
    filterParams: FilterParam[],
  ): TData | null => {
    const dataKey = getDataKey(queryName, filterParams);

    if (!this.data[dataKey]) {
      // if data isn't found, load it
      this.loadData(queryName, filterParams);
      return null;
    }

    return this.data[dataKey]! as TData;
  };

  loadData = async (
    queryName: QueryName,
    filterParams: FilterParam[],
  ): Promise<void> => {
    const dataKey = getDataKey(queryName, filterParams);

    if (this.isLoading[dataKey] || this.isError[dataKey]) {
      return;
    }

    runInAction(() => {
      this.isLoading[dataKey] = true;
    });

    const data = await this.api.getData<ChartData>(queryName, filterParams);

    runInAction(() => {
      if (data) {
        this.data[dataKey] = data;
      } else {
        this.isError[dataKey] = true;
      }

      this.isLoading[dataKey] = false;
    });
  };
}

export const {
  Provider: WidgetDataServiceProvider,
  useContext: useWidgetDataService,
} = createExplicitContext<WidgetDataStore>('WidgetDataStore');

/**
 * Data is stored keyed by query name + a stringified / encoded version of
 * filter params.
 */
type DataStoreKey = `${QueryName}_${string}`;

/**
 * Data is stored keyed by query name + a stringified / encoded version of
 * filter params.
 * @param queryName
 * @param filterParams
 * @returns
 */
function getDataKey(
  queryName: QueryName,
  filterParams: FilterParam[],
): DataStoreKey {
  const filterParamsStr = encodeURIComponent(JSON.stringify(filterParams));
  return `${queryName}_${filterParamsStr}`;
}
