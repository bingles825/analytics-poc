import { Milliseconds, NumberWithUnit, UnitSpec } from '../data';
import { range } from '../util';
import {
  calculateSpanInMs,
  parseUnitSpec,
  unitMillisecondMap,
} from './timeSeries';

beforeEach(() => {
  expect.hasAssertions();
});

describe('parseUnitSpec', () => {
  it.each(['1', 'w', 'garbage'] as NumberWithUnit[])(
    'should throw error if invalid input given',
    (input) => {
      expect(() => parseUnitSpec(input)).toThrow(
        `Invalid unit string ${input}`,
      );
    },
  );

  it.each([...range(1, 30)])(`should parse day unit details: %s`, (value) => {
    const expected: UnitSpec = {
      value,
      unit: 'd',
    };

    const actual = parseUnitSpec(`${value} d`);
    expect(actual).toEqual(expected);
  });

  it.each([...range(1, 52)])(`should parse week unit details: %s`, (value) => {
    const expected: UnitSpec = {
      value,
      unit: 'w',
    };

    const actual = parseUnitSpec(`${value} w`);
    expect(actual).toEqual(expected);
  });

  it.each([...range(1, 12)])(`should parse month unit details: %s`, (value) => {
    const expected: UnitSpec = {
      value,
      unit: 'M',
    };

    const actual = parseUnitSpec(`${value} M`);
    expect(actual).toEqual(expected);
  });

  it.each([...range(1, 10)])(`should parse year unit details: %s`, (value) => {
    const expected: UnitSpec = {
      value,
      unit: 'y',
    };

    const actual = parseUnitSpec(`${value} y`);
    expect(actual).toEqual(expected);
  });
});

describe('calculateSpanInMs', () => {
  it.each([
    [{ unit: 'd', value: 4 }, 4 * unitMillisecondMap.d],
    [{ unit: 'w', value: 9 }, 9 * unitMillisecondMap.w],
    [{ unit: 'M', value: 13 }, 13 * unitMillisecondMap.M],
    [{ unit: 'y', value: 5 }, 5 * unitMillisecondMap.y],
  ] as [UnitSpec, Milliseconds][])(
    'should calculate the span in ms',
    (given, expected) => {
      const actual = calculateSpanInMs(given);
      expect(actual).toEqual(expected);
    },
  );
});
