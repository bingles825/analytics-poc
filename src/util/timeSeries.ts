import { Milliseconds, NumberWithUnit, Unit, UnitSpec } from '../data';

export const daily = {
  hours: 24,
  minutes: 24 * 60,
  seconds: 24 * 60 * 60,
  milliseconds: 24 * 60 * 60 * 1000,
};

/**
 * Map units to the number of milliseconds they contain.
 */
export const unitMillisecondMap: Record<Unit, Milliseconds> = {
  d: daily.milliseconds,
  w: daily.milliseconds * 7,
  M: (daily.milliseconds * 365) / 12, // TODO: figure out if this is the best methodology
  y: daily.milliseconds * 365,
};

export function nDaysDomain(max: number, totalDays: number): [number, number] {
  let min = max;
  for (let i = 0; i < totalDays - 1; i++) {
    min -= daily.milliseconds;
  }

  return [min, max];
}

/**
 * Parses a number with unit string into a spec that can be used for building
 * chart domains and other chart related tasks.
 * @param numberWithUnit
 * @returns
 */
export function parseUnitSpec(numberWithUnit: NumberWithUnit): UnitSpec {
  if (!/\d+ [dwMy]/.test(numberWithUnit)) {
    throw new Error(`Invalid unit string ${numberWithUnit}`);
  }

  const [valueStr, unit] = numberWithUnit.split(' ') as [string, Unit];
  const value = Number(valueStr);

  return {
    value,
    unit,
  };
}

/**
 * Determine the number of milliseconds that span a unit spec.
 */
export function calculateSpanInMs({ unit, value }: UnitSpec): Milliseconds {
  return unitMillisecondMap[unit] * value;
}
