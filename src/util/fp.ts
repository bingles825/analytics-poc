export function times(n: number) {
  return (value: number) => value * n;
}

export function mapArrayKey<K extends number, TFrom, TTo>(
  key: K,
  map: (from: TFrom) => TTo,
) {
  return <T extends TFrom[], R extends (TFrom | TTo)[]>(value: T): R => {
    const copy = [...value] as unknown as R;
    copy[key] = map(value[key]);
    return copy;
  };
}
