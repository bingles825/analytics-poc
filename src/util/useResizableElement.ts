import React from 'react';

export interface UseResizableElementResult {
  element: HTMLElement | null;
  setElement: (element: HTMLElement | null) => void;
  width: number;
  height: number;
}

/**
 * Keep track of the dimensions of a DOM element when a component is mounted to
 * it and when it is resized.
 */
export function useResizableElement(): UseResizableElementResult {
  const [element, setElement] = React.useState<HTMLElement | null>(null);
  const [bounds, setBounds] = React.useState({ width: 0, height: 0 });

  React.useEffect(() => {
    if (!element) {
      return;
    }

    function onResize() {
      if (element) {
        const { width, height } = element.getBoundingClientRect();
        setBounds({ width, height });
      }
    }

    onResize();

    element.addEventListener('resize', onResize);
    return () => element.removeEventListener('resize', onResize);
  }, [element]);

  return {
    ...bounds,
    element,
    setElement,
  };
}
