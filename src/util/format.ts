import { timeFormat } from 'd3-time-format';

export const mmmddFormatter = timeFormat('%b %d');
export const mmmddyyyyFormatter = timeFormat('%b %d, %Y');

export function timeSeriesXAxisFormatter(ms: number) {
  return mmmddFormatter(new Date(ms));
}

export function timeSeriesTooltipLabelFormatter(
  value: string | number,
): string {
  return mmmddyyyyFormatter(new Date(Number(value)));
}
