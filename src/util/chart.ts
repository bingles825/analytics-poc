/**
 * Generates ticks for a given domain spaced at a given interval.
 * Includes the start and end of the domain.
 */
export function* ticks(
  [start, end]: [number, number] | [Date, Date],
  interval: number,
  reverse?: boolean,
) {
  if (interval > 0) {
    if (reverse) {
      for (let i = Number(end); i >= start; i -= interval) {
        yield i;
      }
    } else {
      for (let i = Number(start); i <= end; i += interval) {
        yield i;
      }
    }
  }
}
