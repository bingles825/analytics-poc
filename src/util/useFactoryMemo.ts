import React from 'react';

/**
 * Helper wrapper around React.useMemo for cases where we just want to pass args
 * to a factory function and memoize it until any arg changes.
 */
export function useFactoryMemo<TArgs extends unknown[], TValue>(
  factory: (...args: TArgs) => TValue,
  ...args: TArgs
) {
  return React.useMemo(() => {
    return factory(...args);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [...args]);
}
