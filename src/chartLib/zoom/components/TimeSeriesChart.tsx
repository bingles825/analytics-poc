import React from 'react';
import { styled } from '@mui/material/styles';
import {
  BarSeriesClickData,
  TimeChartData,
  TimeChartDefinition,
} from '../../../data';
import { daily, useResizableElement } from '../../../util';

const columnColors = [
  '#8CC63F',
  hexToRGBAString('#8CC63F', 0.4),
  '#01557F',
  hexToRGBAString('#01557F', 0.4),
];

const lineColors = [
  '#8CC63F',
  '#01557F',
  hexToRGBAString('#8CC63F', 0.4),
  hexToRGBAString('#01557F', 0.4),
];

const now = 1597968000000;

const StyledRoot = styled('div')({
  border: '1px solid #ccc',
});

export interface TimeSeriesChartProps {
  className?: string;
  title: string;
  definition: TimeChartDefinition;
  data: TimeChartData | null;
  aspectRatio: { width: number; height: number };
  onBarClick: (barClickData: BarSeriesClickData<unknown>) => void;
}

const TimeSeriesChart: React.FC<TimeSeriesChartProps> = ({
  className,
  data,
  definition,
  onBarClick,
}) => {
  const { element, setElement } = useResizableElement();

  const onClick = React.useCallback(
    (
      event: ZoomCharts.Configuration.BaseMouseEvent,
      args: ZoomCharts.Configuration.TimeChartChartEventArguments,
    ) => {
      event.preventDefault();

      const { x, y } = event.target.getBoundingClientRect();

      console.log('event:', event);
      console.log('args:', args.hoverSeries.id);
      console.log({ x, y });

      onBarClick({
        dataKey: '',
        value: 0,
        x: 0,
        y: 0,
        clientX: x + event.x,
        clientY: y + event.y,
        element: event.target,
      });
    },
    [onBarClick],
  );

  React.useEffect(() => {
    if (!element) {
      return;
    }

    const values = data?.values ?? [];

    // TODO: For now, slicing the series to only what we find in the data. There
    // seems to be some cases where data needs to be combined to provide for all
    // series
    let series = values.length
      ? createZoomSeries(definition).slice(0, values[0].length - 1)
      : createZoomSeries(definition);

    series = [...expandWithPredictiveSeries(series)];

    new TimeChart({
      ...definition,
      container: element,
      events: {
        onClick,
        // onRightClick: onClick,
      },
      info: {
        showNullData: false,
      },
      chartTypes: {
        columns: {
          style: {
            clusterPadding: [48, 48],
          },
        },
      },
      currentTime: {
        showTime: false,
        time: now + daily.milliseconds,
        style: {
          lineDash: [6, 3],
        },
      },
      style: {
        columnColors,
        lineColors,
      },
      data: [
        {
          units: definition.data.units,
          timestampInSeconds: true,
          preloaded: {
            unit: definition.data.dataUnit,
            values,
          } as ZoomCharts.Configuration.TimeChartDataObject,
        },
      ],
      series,
    });
  }, [data?.values, definition, element, onClick]);

  return (
    <StyledRoot className={className} ref={setElement}>
      TimeSeriesChart
    </StyledRoot>
  );
};

export default TimeSeriesChart;

function createZoomSeries(
  definition: TimeChartDefinition,
): ZoomCharts.Configuration.TimeChartSettingsSeries[] {
  return definition.series.map(
    (
      {
        style: {
          // This desstructuring is to pop off the `desc` field since it is not
          // part of the Zoom charts style api.
          legend: { desc, ...legend },
          ...style
        },
        ...series
      },
      i,
    ) => ({
      ...series,
      style: {
        ...style,
        // fillColor: colorScale[i],
        legend,
      },
    }),
  );
}

function hexToRGB(hex: `#${number | string}`): [number, number, number] {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);

  return [r, g, b];
}

function hexToRGBAString(hex: `#${number | string}`, alpha: number): string {
  const [r, g, b] = hexToRGB(hex);
  return `rgba(${r},${g},${b},${alpha})`;
}

function* expandWithPredictiveSeries(
  series: ZoomCharts.Configuration.TimeChartSettingsSeries[],
): Generator<ZoomCharts.Configuration.TimeChartSettingsSeries, void, unknown> {
  for (const s of series) {
    if (s.type !== 'columns') {
      yield s;
      continue;
    }

    yield {
      ...s,
      data: {
        ...s.data,
        aggregatedValueFunction: (value: number, time: number): any => {
          return time <= now ? value : null;
        },
      },
    };

    yield {
      ...s,
      id: s.id + '_predictive',
      name: `${s.name} (predictive)`,
      stack: s.stack + '_predictive',
      // showInLegend: false,
      data: {
        ...s.data,
        aggregatedValueFunction: (value: number, time: number): any => {
          return value * Math.random() * 1.5;
        },
      },
    };
  }
}
