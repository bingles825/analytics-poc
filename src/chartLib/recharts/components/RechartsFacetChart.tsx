import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Bar,
  BarChart,
  Legend,
  XAxis,
  Tooltip,
  YAxis,
  ResponsiveContainer,
  DataKey,
} from 'recharts';
import {
  BarSeriesClickData,
  FacetChartData,
  FacetChartDefinition,
} from '../../../data';
import { useBarClickCallback } from './useBarClickCallback';

const StyledRoot = styled('div')({});

export interface RechartsFacetChartProps {
  className?: string;
  title: string;
  definition: FacetChartDefinition;
  data: FacetChartData | null;
  onBarClick: (barClickData: BarSeriesClickData<DataKey>) => void;
}

const RechartsFacetChart: React.FC<RechartsFacetChartProps> = ({
  className,
  data,
  definition,
  title,
  onBarClick,
}) => {
  return (
    <StyledRoot className={className}>
      {title}
      <ResponsiveContainer width="99%" height={400}>
        <BarChart data={data?.values.slice(-6)}>
          <XAxis
            dataKey="0"
            padding={{ left: 50, right: 50 }}
            // domain={domain}s
            // ticks={xAxisTicks}
            // tickFormatter={xAxisFormatter}
          />
          <YAxis
            label={{
              angle: -90,
              position: 'insideLeft',
              value: definition.valueAxis.primary.title,
            }}
          />
          <Tooltip
          // labelFormatter={tooltipLabelFormatter}
          // formatter={tooltipFormatter}
          />
          <Legend verticalAlign="bottom" />
          <Bar
            dataKey="1"
            fill="#8CC63F"
            onClick={useBarClickCallback('1', onBarClick)}
          />
          <Bar
            dataKey="2"
            fill="#01557F"
            onClick={useBarClickCallback('2', onBarClick)}
          />
        </BarChart>
      </ResponsiveContainer>
    </StyledRoot>
  );
};

export default RechartsFacetChart;
