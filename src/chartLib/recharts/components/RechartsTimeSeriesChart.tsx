import React from 'react';
import { styled } from '@mui/material/styles';
import {
  Bar,
  BarChart,
  Legend,
  ResponsiveContainer,
  XAxis,
  Tooltip,
  YAxis,
  XAxisProps,
  DataKey,
  CartesianGrid,
} from 'recharts';
import { timeFormat } from 'd3-time-format';
import {
  BarSeriesClickData,
  TimeChartDefinition,
  TimeChartData,
} from '../../../data';
import { ticks } from '../../../util';
import { useBarClickCallback } from './useBarClickCallback';

const domain: [number, number] = [1597708800, 1598313600];
const xAxisTicks = [...ticks(domain, 86400)];
const mmmddFormatter = timeFormat('%b %d');
const mmmddyyyyFormatter = timeFormat('%b %d, %Y');

const StyledRoot = styled('div')({});

const timeSeriesXAxisCommon: XAxisProps = {
  allowDataOverflow: true, // if we don't set this, the domain will be determined by our data instead of what we explicitly set
  interval: 0, // force render all ticks
  scale: 'time',
  type: 'number',
};

export interface RechartsTimeSeriesChartProps {
  className?: string;
  title: string;
  definition: TimeChartDefinition;
  data: TimeChartData | null;
  onBarClick: (barClickData: BarSeriesClickData<DataKey>) => void;
}

const RechartsTimeSeriesChart: React.FC<RechartsTimeSeriesChartProps> = ({
  className,
  title,
  definition,
  data,
  onBarClick,
}) => {
  return (
    <StyledRoot className={className}>
      {title}
      <ResponsiveContainer width="99%" height={400}>
        <BarChart data={data?.values.slice(-6)}>
          <XAxis
            {...timeSeriesXAxisCommon}
            dataKey="0"
            padding={{ left: 50, right: 50 }}
            domain={domain}
            ticks={xAxisTicks}
            tickFormatter={xAxisFormatter}
          />
          <YAxis
            label={{
              angle: -90,
              position: 'insideLeft',
              value: definition.valueAxis.primary.title,
            }}
          />
          <Tooltip
            labelFormatter={tooltipLabelFormatter}
            formatter={tooltipFormatter}
          />
          <CartesianGrid vertical={false} />
          <Legend formatter={legendFormatter} verticalAlign="bottom" />
          <Bar
            dataKey="1"
            fill="#8CC63F"
            onClick={useBarClickCallback('1', onBarClick)}
          />
          <Bar
            dataKey="2"
            fill="#01557F"
            onClick={useBarClickCallback('2', onBarClick)}
          />
        </BarChart>
      </ResponsiveContainer>
    </StyledRoot>
  );
};

export default RechartsTimeSeriesChart;

const labels = ['Created', 'Total visits', 'Total bills'];

function legendFormatter(value: number) {
  return labels[value];
}

function tooltipLabelFormatter(value: string | number): string {
  return mmmddyyyyFormatter(new Date(Number(value) * 1000));
}

function tooltipFormatter(
  value: string | number | Array<string | number>,
  name: string,
): [string | number | Array<string | number>, string] {
  return [value, labels[Number(name)]];
}

function xAxisFormatter(value: number) {
  return mmmddFormatter(new Date(value * 1000));
}
