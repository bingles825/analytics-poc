import React from 'react';
import { DataKey } from 'recharts';
import { BarSeriesClickData } from '../../../data';

/**
 * Custom hook to handle clicks on bars.
 */
export function useBarClickCallback(
  dataKey: DataKey,
  callback: (barClickData: BarSeriesClickData<DataKey>) => void,
) {
  return React.useCallback(
    (
      { value, x, y }: { value: number; x: number; y: number },
      _i: number,
      { currentTarget, clientX, clientY }: React.MouseEvent<Element>,
    ) =>
      callback({
        dataKey,
        value,
        x,
        y,
        clientX,
        clientY,
        element: currentTarget,
      }),
    [callback, dataKey],
  );
}
