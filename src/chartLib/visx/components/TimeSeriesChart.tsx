import React from 'react';
import { styled } from '@mui/material/styles';
import { scaleBand, scaleLinear } from '@visx/scale';
import {
  BarSeriesClickData,
  TimeChartData,
  TimeChartDefinition,
} from '../../../data';
import { TimeSeriesChartState } from '../../../services';
import { Group } from '@visx/group';
import { Bar } from '@visx/shape';
import { useResizableElement } from '../../../util';

const StyledRoot = styled('div')({
  flexGrow: 1,
  minHeight: 400,
});

export interface TimeSeriesChartProps {
  className?: string;
  title: string;
  definition: TimeChartDefinition;
  data: TimeChartData | null;
  aspectRatio: { width: number; height: number };
  onBarClick: (barClickData: BarSeriesClickData<unknown>) => void;
}

const TimeSeriesChart: React.FC<TimeSeriesChartProps> = ({
  className,
  definition,
  data,
}) => {
  const { setElement, width, height } = useResizableElement();

  const chartState = React.useMemo(
    () => new TimeSeriesChartState(definition, data),
    [data, definition],
  );

  const xMax = width;
  const yMax = height;

  console.log(chartState.getXAxisTicks());

  const xScale = React.useMemo(
    () =>
      scaleBand<number>({
        range: [0, xMax],
        round: true,
        domain: chartState.getXAxisTicks().slice(-8),
        padding: 0.4,
      }),
    [chartState, xMax],
  );

  const yScale = React.useMemo(
    () =>
      scaleLinear<number>({
        range: [yMax, 0],
        round: true,
        domain: chartState.getZoomDomain().y,
      }),
    [chartState, yMax],
  );

  return (
    <StyledRoot className={className} ref={setElement}>
      <svg width={width} height={height}>
        <rect width={width} height={height} fill="#ccc" />
        <Group>
          {chartState.getDataZoomed().map((datum) => {
            const [timeStamp, value] = datum;
            const barWidth = xScale.bandwidth();
            const barHeight = yMax - (yScale(value) ?? 0);
            const barX = xScale(timeStamp);
            const barY = yMax - barHeight;

            console.log(timeStamp, barX, barY);

            return (
              <Bar
                key={`bar-${timeStamp}`}
                x={barX}
                y={barY}
                width={barWidth}
                height={barHeight}
              />
            );
          })}
        </Group>
      </svg>
    </StyledRoot>
  );
};

export default TimeSeriesChart;
