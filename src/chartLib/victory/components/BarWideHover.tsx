import React from 'react';
import { styled } from '@mui/material/styles';
import { Bar, BarProps } from 'victory';

const StyledRoot = styled('g', { name: 'BarWideHover' })({});

export interface BarWideHoverProps extends BarProps {
  className?: string;
}

/**
 * Custom bar component to extend the hover area so there aren't dead zones between
 * bars within group on same axis tick.
 */
const BarWideHover: React.FC<BarWideHoverProps> = ({ className, ...props }) => {
  const width = (props.barWidth as number) * 0.8;

  return (
    <StyledRoot>
      <rect
        x={props.x! - width / 2}
        y={props.y}
        width={width as number}
        height={props.y0! - props.y!}
        fill={props.style?.fill}
      />
      <Bar {...props} style={{ ...props.style, fill: 'transparent' }} />
    </StyledRoot>
  );
};

export default BarWideHover;
