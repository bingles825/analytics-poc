import { styled } from '@mui/material/styles';

const StyledRoot = styled('g', { name: 'TooltipFlyout' })({});

export interface TimeSeriesTooltipFlyoutProps {
  scale: number;
  x?: number;
  y?: number;
  width?: number;
  height?: number;
  pointerLength?: number;
  orientation?: string;
}

const TimeSeriesTooltipFlyout: React.FC<TimeSeriesTooltipFlyoutProps> = ({
  scale,
  x,
  y,
  width,
  height,
  pointerLength,
}) => {
  const widthAdj = width! * scale;
  const heightAdj = height! * scale;

  return (
    <StyledRoot>
      <rect
        x={x! - widthAdj! / 2}
        y={y! - heightAdj! - pointerLength! / scale}
        width={widthAdj}
        height={heightAdj}
        fill="#fff"
        stroke="#222"
        strokeWidth="1px"
        vectorEffect="non-scaling-stroke"
      />
    </StyledRoot>
  );
};

export default TimeSeriesTooltipFlyout;
