import React from 'react';
import { styled } from '@mui/material/styles';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryLabel,
  VictoryGroup,
  VictoryTooltip,
  VictoryZoomContainer,
  VictoryLine,
  VictoryScatter,
} from 'victory';
import {
  BarSeriesClickData,
  SeriesDefinitionType,
  TimeChartData,
  TimeChartDefinition,
} from '../../../data';
import {
  timeSeriesTooltipLabelFormatter,
  timeSeriesXAxisFormatter,
} from '../../../util';
import { observer } from 'mobx-react-lite';

import { BarWideHover, TimeSeriesTooltipFlyout } from '.';
import { TimeSeriesChartState } from '../../../services';

const colorScale = ['#8CC63F', '#01557F'];

const StyledRoot = styled('div')({});

export interface TimeSeriesChartProps {
  className?: string;
  title: string;
  definition: TimeChartDefinition;
  data: TimeChartData | null;
  aspectRatio: { width: number; height: number };
  onBarClick: (barClickData: BarSeriesClickData<unknown>) => void;
}

const TimeSeriesChart: React.FC<TimeSeriesChartProps> = ({
  className,
  title,
  data,
  definition,
  aspectRatio,
  onBarClick,
}) => {
  const chartState = React.useMemo(
    () => new TimeSeriesChartState(definition, data),
    [data, definition],
  );
  const dataZoomed = chartState.getDataZoomed();
  const barData = chartState.getBarData();

  const today = 1597968000000;

  const xDomain = chartState.getXDomain();
  const xDomainMax = xDomain?.[1];

  const [containerEl, setContainerEl] = React.useState<HTMLElement | null>(
    null,
  );

  const containerWidth = chartState.getContainerWidth();
  const scale = containerWidth > 0 ? aspectRatio.width / containerWidth : 0; // 450 / 911;
  const tickLabelFontSize = 16 * scale;

  React.useEffect(() => {
    if (!containerEl) {
      return;
    }

    function onResize() {
      const xAxisEl = containerEl?.querySelector('svg > g.xAxis');
      chartState.setContainerWidth(xAxisEl?.getBoundingClientRect().width ?? 0);
    }

    onResize();

    containerEl.addEventListener('resize', onResize);
    return () => containerEl.removeEventListener('resize', onResize);
  }, [chartState, containerEl]);

  const onClick = React.useCallback(
    (event: React.MouseEvent) => {
      const { currentTarget, clientX, clientY } = event;
      event.preventDefault();
      onBarClick({
        dataKey: '',
        value: 0,
        x: 0,
        y: 0,
        clientX,
        clientY,
        element: currentTarget,
      });
    },
    [onBarClick],
  );

  const xAxis = (
    <VictoryAxis
      groupComponent={<g className="xAxis" role="presentation" />}
      scale="time"
      style={{
        ticks: { stroke: '#ccc', strokeWidth: 2, size: 3 },
      }}
      tickFormat={timeSeriesXAxisFormatter}
      tickLabelComponent={
        <VictoryLabel style={{ fontSize: tickLabelFontSize }} />
      }
      tickValues={chartState.getXAxisTicks()}
    />
  );

  const yAxis = (
    <VictoryAxis
      dependentAxis
      groupComponent={<g className="yAxis" role="presentation" />}
      style={{
        grid: { stroke: '#ccc' },
        ticks: { stroke: '#ccc', strokeWidth: 2, size: 3 },
      }}
      tickLabelComponent={
        <VictoryLabel style={{ fontSize: tickLabelFontSize }} />
      }
      tickValues={chartState.getYAxisTicks()}
    />
  );

  const seriesDefs = definition.series.reduce(
    (seriesDefs, def) => {
      seriesDefs[def.type].push(def);
      return seriesDefs;
    },
    { columns: [], line: [] } as Record<
      SeriesDefinitionType['type'],
      SeriesDefinitionType[]
    >,
  );

  const barSeries = seriesDefs.columns.slice(0, 2).map(({ name }, i) => (
    <VictoryBar
      key={i}
      x="0"
      y={i + 1}
      barWidth={scale > 0 ? barData.barWidth / scale : 0}
      data={dataZoomed}
      dataComponent={
        <BarWideHover
          events={{
            onContextMenu: onClick,
          }}
        />
      }
      labels={({ datum }) => {
        return [
          timeSeriesTooltipLabelFormatter(datum[0]),
          `${name}: ${datum[i + 1]}`,
        ];
      }}
      labelComponent={
        <VictoryTooltip
          flyoutComponent={<TimeSeriesTooltipFlyout scale={scale} />}
          labelComponent={
            <VictoryLabel style={{ fontSize: tickLabelFontSize }} />
          }
        />
      }
      style={{
        data: {
          opacity: ({ datum }) => (datum[0] >= today ? 0.5 : 1),
        },
      }}
    />
  ));

  const lineSeries = seriesDefs.line.map(({ name }, i) => (
    <VictoryLine
      key={i}
      style={{
        data: {
          stroke: colorScale[i],
          opacity: ({ datum }) => (datum?.[0] >= today ? 0.5 : 1),
        },
      }}
      x="0"
      y={i + 1}
      data={dataZoomed}
      interpolation="monotoneX"
    />
  ));

  const scatterSeries = seriesDefs.line.map(({ name }, i) => (
    <VictoryScatter
      key={i}
      style={{ data: { fill: colorScale[i] } }}
      x="0"
      y={i + 1}
      data={dataZoomed}
      labels={({ datum }) => {
        return [
          timeSeriesTooltipLabelFormatter(datum[0]),
          `${name}: ${datum[i + 1]}`,
        ];
      }}
      labelComponent={
        <VictoryTooltip
          flyoutComponent={<TimeSeriesTooltipFlyout scale={scale} />}
          labelComponent={
            <VictoryLabel style={{ fontSize: tickLabelFontSize }} />
          }
        />
      }
    />
  ));

  return (
    <StyledRoot ref={setContainerEl} className={className}>
      {title}
      <VictoryChart
        width={aspectRatio.width}
        height={aspectRatio.height}
        containerComponent={
          <VictoryZoomContainer
            zoomDimension="x"
            zoomDomain={chartState.getZoomDomain()}
            onZoomDomainChange={chartState.onZoomDomainChange}
          />
        }
        domain={xDomain && { x: xDomain }}
        maxDomain={{ x: xDomainMax && Number(xDomainMax) }}>
        {xAxis}
        {yAxis}
        <VictoryGroup offset={barData.groupOffset} colorScale={colorScale}>
          {barSeries}
        </VictoryGroup>
        {lineSeries}
        {scatterSeries}
      </VictoryChart>
    </StyledRoot>
  );
};

export default observer(TimeSeriesChart);
