import React from 'react';
import { ResponsiveBar } from '@nivo/bar';
import { styled } from '@mui/material/styles';
import { timeFormat } from 'd3-time-format';
import {
  BarSeriesClickData,
  TimeChartData,
  TimeChartDefinition,
} from '../../../data';
import { ticks, useFactoryMemo } from '../../../util';

const domain: [number, number] = [1597708800, 1598313600];
const xAxisTicks = [...ticks(domain, 86400)];
// const yAxisTicks = [...ticks([0, 180], 20)];

const mmmddFormatter = timeFormat('%b %d');
// const mmmddyyyyFormatter = timeFormat('%b %d, %Y');

const StyledRoot = styled('div')({
  height: 400,
});

export interface NivoTimeSeriesChartProps {
  className?: string;
  title: string;
  definition: TimeChartDefinition;
  data: TimeChartData | null;
  onBarClick: (barClickData: BarSeriesClickData<unknown>) => void;
}

type ResponsiveBarProps = Parameters<typeof ResponsiveBar>[0];

const NivoTimeSeriesChart: React.FC<NivoTimeSeriesChartProps> = ({
  className,
  data: dataRaw,
  definition,
  onBarClick,
  title,
}) => {
  const data = useFactoryMemo(transformData, dataRaw);

  const axisProps = useFactoryMemo(
    createAxisProps,
    definition.valueAxis.primary.title,
  );
  const barProps = useFactoryMemo(createBarProps);
  const dataProps = useFactoryMemo(createDataProps, data);
  const layoutProps = useFactoryMemo(createLayoutProps);
  const legendProps = useFactoryMemo(createLegendProps);
  const tooltipProps = useFactoryMemo(createTooltipProps);

  const onClick: ResponsiveBarProps['onClick'] = React.useCallback(
    ({ id, value }, { currentTarget, clientX, clientY }) => {
      onBarClick({
        dataKey: id,
        value: value ?? 0,
        x: 0,
        y: 0,
        clientX,
        clientY,
        element: currentTarget,
      });
    },
    [onBarClick],
  );

  return (
    <StyledRoot className={className}>
      {title}
      <ResponsiveBar
        {...axisProps}
        {...barProps}
        {...dataProps}
        {...layoutProps}
        {...legendProps}
        {...tooltipProps}
        maxValue={180}
        onClick={onClick}
      />
    </StyledRoot>
  );
};

export default NivoTimeSeriesChart;

const labels = ['Created', 'Total visits', 'Total bills'];

function labelFormatter(value: number) {
  return labels[value];
}

function xAxisFormatter(value: number) {
  return mmmddFormatter(new Date(value * 1000));
}

function transformData(raw: TimeChartData | null) {
  // Create a map of datums to make it easy to associate them with axis ticks
  const map =
    raw?.values.slice(-6).reduce((map, [a, b, c]) => {
      map[a] = { 1: b, 2: c };
      return map;
    }, {} as Record<string, { 1: number; 2: number }>) ?? {};

  // Since we may have gaps in our data, we use the xAxisTicks to define
  // the datums
  return xAxisTicks.map((tick) => ({
    0: tick,
    ...map[tick],
  }));
}

function createAxisProps(xAxisLabel: string): Partial<ResponsiveBarProps> {
  return {
    axisLeft: {
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      // tickValues: yAxisTicks,
      legend: xAxisLabel,
      legendPosition: 'middle',
      legendOffset: -40,
    },
    axisBottom: {
      tickSize: 5,
      tickPadding: 5,
      tickRotation: 0,
      format: xAxisFormatter,
      legendPosition: 'middle',
      legendOffset: 32,
    },
  };
}

function createBarProps(): Partial<ResponsiveBarProps> {
  return {
    enableLabel: false,
    groupMode: 'grouped',
    innerPadding: 4,
    padding: 0.4,
    colors: ['#8CC63F', '#01557F'],
  };
}

function createDataProps(
  data: ResponsiveBarProps['data'],
): Pick<ResponsiveBarProps, 'data' | 'indexBy' | 'keys'> {
  return {
    indexBy: '0',
    keys: ['1', '2'],
    data,
  };
}

function createLayoutProps(): Partial<ResponsiveBarProps> {
  return {
    margin: {
      top: 6,
      bottom: 60,
      left: 60,
      right: 0,
    },
  };
}

function createLegendProps(): Partial<ResponsiveBarProps> {
  return {
    legendLabel: ({ id }) => labelFormatter(Number(id)),
    legends: [
      {
        dataFrom: 'keys',
        anchor: 'bottom',
        direction: 'row',
        translateX: 0,
        translateY: 50,
        itemsSpacing: 2,
        itemWidth: 100,
        itemHeight: 20,
        itemDirection: 'left-to-right',
        itemOpacity: 0.85,
        symbolSize: 20,
        effects: [
          {
            on: 'hover',
            style: {
              itemOpacity: 1,
            },
          },
        ],
      },
    ],
  };
}

function createTooltipProps(): Partial<ResponsiveBarProps> {
  return {
    tooltipLabel: ({ id }) => labelFormatter(Number(id)),
  };
}
