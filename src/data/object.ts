import {
  WidgetDefinitionName,
  WidgetDrilldownDefinitionName,
  WidgetName,
} from './widget';

export type ObjectName =
  | WidgetName
  | WidgetDefinitionName
  | WidgetDrilldownDefinitionName;

export type ObjectType = 'Widget' | 'Definition';
