import { WidgetNamePrefix } from './name';

export type DrilldownDimension =
  | 'Transactions'
  | 'Specialty'
  | 'Provider'
  | 'Practice'
  | 'PayerName'
  | 'PayerCategory'
  | 'Facility';

export interface FilterParam {
  name:
    | 'SourceSystem'
    | 'Enterprise'
    | 'Practice'
    | 'Facility'
    | 'Speciality'
    | 'Provider'
    | 'Time';
  type: 'multi' | 'dateRange';
  value: string[];
}

export type DrilldownDimensionQueryName =
  `${WidgetNamePrefix}${DrilldownDimension}Query`;

export type DrilldownQueryName =
  `${WidgetNamePrefix}${'DrillDownby'}${DrilldownDimension}Query`;

export type QueryName = DrilldownDimensionQueryName | DrilldownQueryName;
