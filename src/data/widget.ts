import { WidgetNamePrefix } from './name';
import { DrilldownDimension, FilterParam } from './query';
import { FacetChartDefinition } from './facetChart';
import { TimeChartDefinition } from './timeChart';

export type WidgetName = `${WidgetNamePrefix}${'Widget'}`;
export type WidgetDefinitionName = `${WidgetNamePrefix}${'Def'}`;
export type WidgetDrilldownDefinitionName =
  `${WidgetNamePrefix}DrillDownby${DrilldownDimension}Def`;

export interface WidgetObject {
  type: 'Widget';
  title: string;
  description: string;
  definition: WidgetDefinitionName;
  name: WidgetName;
  minWidth: number;
  visible: boolean;
}

export interface WidgetDefinition {
  drilldown: {
    prompt: string;
    def: WidgetDrilldownDefinitionName;
    param?: DrilldownDimension; // TBD: What does this look like after multiple drilldowns
  }[];
  filters: '12MNonFinancialToDay';
  defType: WidgetDefinitionType;
  defSubType: WidgetDefinitionSubType;
  def: TimeChartDefinition | FacetChartDefinition;
  name: WidgetDefinitionName | WidgetDrilldownDefinitionName;
  type: 'Definition';
  views?: []; // TBD: what is this?
}

export type WidgetDefinitionType = 'ZoomChart';
export type WidgetDefinitionSubType = 'TimeChart' | 'FacetChart';

/**
 * Each widget has a "stack" of 1-n views of data. This interface represents
 * one view in the stack.
 */
export interface WidgetStackItem {
  definitionName: WidgetDefinitionName | WidgetDrilldownDefinitionName;
  filters: FilterParam[];
}
