export type Unit = 'd' | 'w' | 'M' | 'y';
export type NumberWithUnit = `${number} ${Unit}`;
export type Milliseconds = number;

export interface UnitSpec {
  value: number;
  unit: Unit;
}
