import { NumberWithUnit, Unit } from './unit';
import { QueryName } from './query';
import { ColumnSeriesDefinition, LineSeriesDefinition } from './series';

export interface TimeChartDefinition {
  data: {
    query: QueryName;
    dataUnit: 'd';
    units: Unit[];
    sortOptions: {
      sortField: '';
      sortOrder: ['asce', 'desc'];
    };
  };
  legend: {
    enabled: boolean;
  };
  toolbar: {
    fullscreen: boolean;
    logScale: boolean;
  };
  area: {
    displayUnits: { unit: NumberWithUnit; name: string }[];
  };
  navigation: {
    initialDisplayUnit: NumberWithUnit;
    initialDisplayPeriod: NumberWithUnit;
  };
  series: (ColumnSeriesDefinition | LineSeriesDefinition)[];
  valueAxis: {
    primary: {
      side: 'left';
      title: string;
      size: number;
    };
  };
}

export interface TimeChartData {
  querySchema: ({ type: 'series' } | { aggregate: 'sum' })[];
  executionTime: number;
  schema: Record<
    string,
    { name: string; index: number; type: 'Int32' | 'DateTime' }
  >;
  values: number[][];
}
