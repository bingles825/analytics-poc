export type Aggregation = 'avg' | 'sum';

export interface ColumnSeriesDefinition {
  id: string;
  name: string;
  type: 'columns';
  valueAxis: 'primary';
  stack: `default${number}`;
  data: {
    index: number;
    aggregation: Aggregation;
  };
  style: {
    lineWidth: number;
    padding: [number, number];
    legend: {
      marker: {
        shape: 'circle';
      };
      desc: string;
    };
  };
}

export interface LineSeriesDefinition {
  id: string;
  name: string;
  idalias: string;
  type: 'line';
  valueAxis: 'primary';
  data: {
    index: number;
    aggregation: Aggregation;
  };
  style: {
    lineWidth: number;
    smoothing: boolean;
    marker: {
      shape: 'circle';
    };
    legend: {
      desc: string;
    };
  };
}
