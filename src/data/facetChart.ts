import { QueryName } from './query';

export interface FacetChartDefinition {
  data: {
    query: QueryName;
    autoCategories: ['PracticeCategory'];
    sortOptions: {
      sortField: '';
      sortOrder: 'desc';
    };
  };
  toolbar: {
    fullscreen: boolean;
  };
  series: FacetSeriesDefinition[];
  valueAxis: {
    primary: {
      side: 'left';
      title: string;
      valueFormat: '0';
    };
  };
}

export interface FacetSeriesDefinition {
  id: string;
  name: string;
  valueAxis: 'primary';
  stack: 'default' | `default${number}`;
  data: {
    field: string;
    aggregation: 'sum';
  };
  style: {
    lineWidth: number;
    padding: [number, number];
    legend: {
      marker: {
        shape: 'circle';
      };
    };
  };
}

export interface FacetChartData {
  executionTime: number;
  schema: Record<
    string,
    { name: string; index: number; type: 'Int32' | 'DateTime' }
  >;
  values: (string | number)[][];
}
