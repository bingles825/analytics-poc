import { FacetChartData } from './facetChart';
import { TimeChartData, TimeChartDefinition } from './timeChart';

export interface BarSeriesClickData<DataKey> {
  // TODO: determine if we need all of these props
  dataKey: DataKey;
  element: Element;
  value: number;
  x: number;
  y: number;
  clientX: number;
  clientY: number;
}

export type ChartData = FacetChartData | TimeChartData;
export type SeriesDefinitionType = TimeChartDefinition['series'][0];
