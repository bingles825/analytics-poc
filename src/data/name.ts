// TODO: This will likely not be able to stay statically defined. At some point
// we may want to switch this to an alias for string (type WidgetNamePrefix = string)
export type WidgetNamePrefix =
  | 'BillLagandSubmissionlag'
  | 'DailyBilllVolumevsCompletedvsPending'
  | 'DailyVisitsAndTotalBills'
  | 'UnbilledCharges';
